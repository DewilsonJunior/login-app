<!DOCTYPE html>

<html lang="pt-br">
<head>
    <meta charset="utf-8" />

    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap-grid.min.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.min.css" />

    <?php 
        session_start();

        if( (!isset($_SESSION["login"]) == true) and (!isset($_SESSION["senha"]) == true) ){
            unset($_SESSION["login"]);
            unset($_SESSION["senha"]);
            header("location:index.php");
            exit();
        }

        $session = session_name() . "=". session_id();

    ?>

    <title>Quiz App</title>

</head>
<body>
    <div class="container mt-5">

        <h1></h1>

        <div class="d-flex justify-content-center row">
            <div class="col-md-10" id="layout">

                <!-- Modal -->
                <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Parabens Você está logado.</h5>
                            </div>
                            <div class="modal-body d-flex flex-wrap justify-content-between">
                                <p class=""> Sua sessão é: <?php echo($session) ?> </p>
                                <button type="button" data-dismiss="modal" class="btn btn-primary btn-lg px-5 py-3" id="btn-modal">
                                    Parabens!
                                </button>
                                <button type="button" class="btn btn-secondary btn-lg px-5 py-3" id="btn-sair">
                                    Sair
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Fim Modal-->
            </div>
        </div>
    </div>

    
    <script type="text/javascript" src="js/jquery.3.2.1.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="js/home.js"></script>
</body>
</html>
