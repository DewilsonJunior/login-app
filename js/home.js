$("body").ready(function(){

    $('#modal').modal();
});
    

$("#btn-sair").click(function(){
    $.post("usuarioFunc.php",{action: "sair"}, function(result){

        console.log(result);

        if(result == "saida_efetuado_com_sucesso"){
            window.location.href = "index.php";
        }
        else if(result == "error_session"){
            alert("Ocorreu um erro ao tentar sair do sistema, tente novamente." + 
                    "caso o erro persista entre em contato com o time de suporte.");
        }
        else{
            alert("Ocorreu um erro ao tentar sair do sistema por favor, entre em contato com o time de suporte.");
        }
    });
});

