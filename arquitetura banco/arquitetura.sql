CREATE TABLE `quiz_database`.`usuarios` ( 
    `id` INT NOT NULL AUTO_INCREMENT, 
    `login` VARCHAR(100) NOT NULL, 
    `senha` VARCHAR(100) NOT NULL, 
    `email` VARCHAR(100) NOT NULL,
    `created` DATE NOT NULL , 
    `updated` DATE NOT NULL, 
    `deleted` VARCHAR(1) NOT NULL, 
    PRIMARY KEY (`id`)
) ENGINE = InnoDB;